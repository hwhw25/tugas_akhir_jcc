<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('home');
});

Route::group(['middleware' => ['auth']], function(){
    //CRUD Kategori
    //create data
    Route::get('/kategori/create', 'KategoriController@create');
    //menyimpan data ke database
    Route::post('/kategori', 'KategoriController@store');
    //menampilkan data
    Route::get('/kategori', 'KategoriController@index');
    //menampilkan detail data
    Route::get('/kategori/{kategori_id}', 'KategoriController@show');
    //menampilkan form edit data
    Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit');
    //menyimpan perubahan data
    Route::put('/kategori/{kategori_id}', 'KategoriController@update');
    //menghapus data
    Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy');


    // CRUD Departemen
    //menampilkan data
    Route::get('/departemen', 'DepartemenController@index');
    //create data
    Route::get('/departemen/create', 'DepartemenController@create');
    //menyimpan data ke database
    Route::post('/departemen', 'DepartemenController@store');
    //menampilkan detail data
    Route::get('/departemen/{departemen_id}', 'DepartemenController@show');
    //menampilkan form edit data
    Route::get('/departemen/{departemen_id}/edit', 'DepartemenController@edit');
    //menyimpan perubahan data
    Route::put('/departemen/{departemen_id}', 'DepartemenController@update');
    //menghapus data
    Route::delete('/departemen/{departemen_id}', 'DepartemenController@destroy');
  


    //CRUD Barang
    Route::resource('barang', 'BarangController');
});



Auth::routes();

