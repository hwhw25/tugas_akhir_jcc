<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departemen extends Model
{

    protected $table="departemen";
    protected $fillable =["nama_departemen","telepon","user_id","type"];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
 
    public function permintaan()
    {
        return $this->hasOne('App\Permintaan');
    }

}
