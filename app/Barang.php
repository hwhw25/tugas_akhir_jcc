<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table="barang";
    protected $fillable = ["nama_barang","kategori_id"];

    public function kategori()
    {
        return $this->belongsTo('App\Kategori', 'kategori_id');
    }

}
