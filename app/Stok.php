<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stok extends Model
{
    protected $table="stok";
    protected $fillable =["departemen_id","barang_id","jumlah_stok"];

    public function detail_permintaan()
    {
        return $this->hasOne('App\DetailPermintaan');
    }

}
