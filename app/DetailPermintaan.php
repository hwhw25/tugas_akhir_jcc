<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPermintaan extends Model
{
    protected $table="detaildepartemen";
    protected $fillable =["permintaan_id","stok_id","jumlah_permintaan"];

    public function permintaan()
    {
        return $this->belongsTo('App\Permintaan');
    }

    public function stok()
    {
        return $this->belongsTo('App\Stok');
    }

}
