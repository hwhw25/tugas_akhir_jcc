<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Kategori;

class KategoriController extends Controller
{
    public function index(){
        $kategori = DB::table('kategori')->get();
 
        return view('kategori.index', ['kategori' => $kategori]);
    }

    public function create(){
        return view('kategori.create');
    }

    public function store(Request $request){
        $request->validate(
            [
                'nama_kategori' => 'required'
            ],
            [
                'nama_kategori.required' => 'Inputan Nama Kategori Harus Diisi'
            ]
        );

        DB::table('kategori')->insert(
            [
                'nama_kategori' => $request['nama_kategori']
            ]
        );

        return redirect('/kategori');
    }

    public function show($id){
        $kategori = Kategori::findOrFail($id);

        return view('kategori.show', compact('kategori'));
    }

    public function edit($id){
        $kategori = DB::table('kategori')->where('id', $id)->first();

        return view('kategori.edit', compact('kategori'));
    }

    public function update($id, Request $request){
        $request->validate(
            [
                'nama_kategori' => 'required'
            ],
            [
                'nama_kategori.required' => 'Inputan Nama Kategori Harus Diisi'
            ]
        );

        DB::table('kategori')->where('id', $id)
            ->update(
                [
                    'nama_kategori' => $request['nama_kategori']
                ]
            );
        
        return redirect('/kategori');
    }

    public function destroy($id){
        DB::table('kategori')->where('id', '=', $id)->delete();
        return redirect('/kategori');
    }
}
