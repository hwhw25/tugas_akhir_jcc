<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Departemen;
use App\User;

class DepartemenController extends Controller
{
    public function index()
    {
        $departemen = Departemen::all();
        return view('departemen.index', ['departemen' => $departemen],compact('departemen'));

    }

    public function create()
    {
        $user = User::all();

        return view('departemen.create', compact('user'));
    }

    public function store(Request $request){
        $request->validate(
            [
                'nama_departemen' => 'required',
                'telepon' => 'required',
                'user_id' => 'required',
                'type' => 'required',
            ],
            [
                'nama_departemen.required' => 'Inputan Nama Departemen Harus Diisi',
                'telepon.required' => 'Inputan Telepon Harus Diisi',
                'user_id.required' => 'Inputan Nama User Harus Diisi',
                'type.required'  => 'Inputan Type Harus Diisi',
            ]
        );

        $departemen = new Departemen;
        $departemen->nama_departemen = $request->nama_departemen;
        $departemen->telepon = $request->telepon;
        $departemen->user_id = $request->user_id;
        $departemen->type = $request->type;
        $departemen->save();

        return redirect('/departemen');
    }

    public function show($id)
    {
        $departemen = Departemen::findOrFail($id);

        return view('departemen.show', compact('departemen'));
    }

    public function edit($id)
    {
        $departemen = Departemen::findOrFail($id);
        $user = User::all();

        return view('departemen.edit', compact('departemen', 'user'));
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'nama_departemen' => 'required',
                'telepon' => 'required',
                'user_id' => 'required',
                'type' => 'required',
            ],
            [
                'nama_departemen.required' => 'Inputan Nama Departemen Harus Diisi',
                'telepon.required' => 'Inputan Telepon Harus Diisi',
                'user_id.required' => 'Inputan Nama User Harus Diisi',
                'type.required'  => 'Inputan Type Harus Diisi',
            ]
        );

        $departemen = Departemen::find($id);
        $departemen->nama_departemen = $request->nama_departemen;
        $departemen->telepon = $request->telepon;
        $departemen->user_id = $request->user_id;
        $departemen->type = $request->type;

        $departemen->save();
        return redirect('/departemen');
    }

    public function destroy($id)
    {
        $departemen = Departemen::find($id);
 
        $departemen->delete();
        return redirect('/departemen');
    }



}
