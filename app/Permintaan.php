<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permintaan extends Model
{
    protected $table="permintaan";
    protected $fillable =["tanggal","status","departemen_id","target_departemen_id"];

    public function permintaan()
    {
        return $this->hasOne('App\DetailPermintaan');
    }

    public function departemen()
    {
        return $this->belongsTo('App\Departemen');
    }

}
