@extends('layout.master')
@section('judul')
    Halaman List Departemen
@endsection
@section('content')

  <a href="/departemen/create" class="btn btn-success my-3">Tambah Departemen</a>

  <table class="table">
    <thead class="thead-dark" align="center">
      <tr>
        <th scope="col" width="10%">No</th>
        <th scope="col" width="25%">Nama Departemen</th>
        <th scope="col" width="10%">Telepon</th>
        <th scope="col" width="20%">Nama User</th>
        <th scope="col" width="15%">Type</th>
        <th scope="col" width="30%">Action</th>
      </tr>
    </thead>
    <tbody align="center">
        @forelse ($departemen as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama_departemen}}</td>
                <td>{{$item->telepon}}</td>
                <td>{{$item->user->name}}</td>
                <td>{{$item->type}}</td>
                <td>
                    <form action="/departemen/{{$item->id}}" method="POST">
                      @csrf
                      @method('delete')
                      <a href="/departemen/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                      <a href="/departemen/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                      <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <h1>Data Kosong</h1>
        @endforelse
    </tbody>
  </table>
@endsection