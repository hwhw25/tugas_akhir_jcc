@extends('layout.master')
@section('judul')
    Halaman Edit Departemen
@endsection
@section('content')
<form action="/departemen/{{$departemen->id}}" method="POST">
@csrf
@method('put')
    <div class="form-group">
      <label>Nama Departemen</label>
      <input type="text" value="{{$departemen->nama_departemen}}"class="form-control" name="nama_departemen" autofocus>
    </div>
    @error('nama_departemen')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <label>Telepon</label>
      <input type="text" value="{{$departemen->telepon}}"class="form-control" name="telepon" autofocus>
    </div>
    @error('telepon')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <label>Nama User</label>
      <select name="user_id" id="" class="form-control">
          <option value="">--Pilih Nama User--</option>
          @foreach ($user as $item)
              <option value="{{$item->id}}">{{$item->name}}</option>
          @endforeach
      </select>
    </div>  

    @error('user_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
    <label>Type</label>
    <select name="type" id="" class="form-control">
        <option value="">--Pilih Kategori--</option>
        <option value="Gudang">--Gudang--</option>
        <option value="Cabang">--Cabang--</option>
    </select>
  </div>

  @error('type')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror


    <button type="submit" class="btn btn-primary">Submit</button>
@endsection