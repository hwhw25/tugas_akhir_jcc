@extends('layout.master')
@section('judul')
    Halaman Detail Departemen
@endsection

@section('content')
    <div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label>Nama Departemen</label>
                    <input type="text" class="form-control" name="nama_departemen" value="{{$departemen->nama_departemen}}" readonly>
                </div>
                <div class="form-group">
                    <label>Telepon</label>
                    <input type="text" class="form-control" name="telepon" value="{{$departemen->telepon}}" readonly>
                </div>
                <div class="form-group">
                    <label>Nama User</label>
                    <input type="text" class="form-control" name="user_id" value="{{$departemen->user->name}}" readonly>
                </div>
                <div class="form-group">
                    <label>Type</label>
                    <input type="text" class="form-control" name="type" value="{{$departemen->type}}" readonly>
                </div>
                <div class="form-group">
                    <label>Created_at</label>
                    <input type="text" class="form-control" name="telepon" value="{{$departemen->created_at}}" readonly>
                </div>
                <div class="form-group">
                    <label>Updated_at</label>
                    <input type="text" class="form-control" name="user_id" value="{{$departemen->updated_at}}" readonly>
                </div>
                <a href="/departemen" class="btn btn-primary">Kembali</a>
            </div>
        </div>
    </div>
@endsection