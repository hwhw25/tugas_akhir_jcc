@extends('layout.master')
@section('judul')
    Halaman Detail Kategori
@endsection

@section('content')
    <div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label>Nama Kategori</label>
                    <input type="text" class="form-control" name="nama_departemen" value="{{$kategori->nama_kategori}}" readonly>
                </div>
                <div class="form-group">
                    <label>Created_at</label>
                    <input type="text" class="form-control" name="telepon" value="{{$kategori->created_at}}" readonly>
                </div>
                <div class="form-group">
                    <label>Updated_at</label>
                    <input type="text" class="form-control" name="user_id" value="{{$kategori->updated_at}}" readonly>
                </div>
                <a href="/kategori" class="btn btn-primary">Kembali</a>
            </div>
        </div>
    </div>
@endsection