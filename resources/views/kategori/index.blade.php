@extends('layout.master')
@section('judul')
    Halaman List Kategori
@endsection
@section('content')

  <a href="/kategori/create" class="btn btn-success my-3">Tambah Kategori</a>

  <table class="table">
    <thead class="thead-dark" align="center">
      <tr>
        <th scope="col" width="10%">No</th>
        <th scope="col" width="70%">Nama Kategori</th>
        <th scope="col" width="20%">Action</th>
      </tr>
    </thead>
    <tbody align="center">
        @forelse ($kategori as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama_kategori}}</td>
                <td>
                    <form action="/kategori/{{$item->id}}" method="POST">
                      @csrf
                      @method('delete')
                      <a href="/kategori/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                      <a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                      <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <h1>Data Kosong</h1>
        @endforelse
    </tbody>
  </table>
@endsection