@extends('layout.master')
@section('judul')
    Halaman Tambah Kategori
@endsection
@section('content')
<form action="/kategori" method="POST">
@csrf
    <div class="form-group">
      <label>Nama Kategori</label>
      <input type="text" class="form-control" name="nama_kategori" autofocus autocomplete="off" value="{{ old('nama_kategori')}}">
    </div>
    @error('nama_kategori')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
@endsection