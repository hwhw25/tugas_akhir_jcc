@extends('layout.master')
@section('judul')
    Halaman Edit Kategori
@endsection
@section('content')
<form action="/kategori/{{$kategori->id}}" method="POST">
@csrf
@method('put')
    <div class="form-group">
      <label>Nama Kategori</label>
      <input type="text" value="{{$kategori->nama_kategori}}"class="form-control" name="nama_kategori" autofocus>
    </div>
    @error('nama_kategori')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
@endsection