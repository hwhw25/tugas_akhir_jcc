@extends('layout.master')
@section('judul')
    Halaman Edit Barang
@endsection

@section('content')
    <form action="/barang/{{$barang->id}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama barang</label>
            <input type="text" value="{{$barang->nama_barang}}" class="form-control" name="nama_barang">
        </div>

        @error('nama_barang')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Kategori barang</label>
            <select name="kategori_id" id="" class="form-control">
                <option value="">--Pilih Kategori--</option>
                @foreach ($kategori as $item)
                    @if ($item->id === $barang->kategori_id)
                        <option value="{{$item->id}}" selected>{{$item->nama_kategori}}</option>
                    @else
                        <option value="{{$item->id}}">{{$item->nama_kategori}}</option>
                    @endif
                    
                @endforeach
            </select>
        </div>

        @error('kategori_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection