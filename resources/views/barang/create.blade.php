@extends('layout.master')
@section('judul')
    Halaman Tambah Barang
@endsection

@section('content')
    <form action="/barang" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label>Nama Barang</label>
            <input type="text" class="form-control" name="nama_barang" autofocus value="{{ old('nama_barang') }}">
        </div>

        @error('nama_barang')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        
        <div class="form-group">
            <label>Kategori Barang</label>
            <select name="kategori_id" id="" class="form-control">
                <option value="">--Pilih Kategori--</option>
                @foreach ($kategori as $item)
                    <option value="{{$item->id}}">{{$item->nama_kategori}}</option>
                @endforeach
            </select>
        </div>

        @error('kategori_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection