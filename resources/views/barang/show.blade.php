@extends('layout.master')
@section('judul')
    Halaman Detail Barang
@endsection

@section('content')
<div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label>Nama Barang</label>
                    <input type="text" class="form-control" value="{{$barang->nama_barang}}" readonly>
                </div>
                <div class="form-group">
                    <label>Nama Kategori</label>
                    <input type="text" class="form-control" value="{{$barang->kategori->nama_kategori}}" readonly>
                </div>
                <div class="form-group">
                    <label>Created_at</label>
                    <input type="text" class="form-control" value="{{$barang->created_at}}" readonly>
                </div>
                <div class="form-group">
                    <label>Updated_at</label>
                    <input type="text" class="form-control" value="{{$barang->updated_at}}" readonly>
                </div>
                <a href="/barang" class="btn btn-primary">Kembali</a>
            </div>
        </div>
    </div>@endsection