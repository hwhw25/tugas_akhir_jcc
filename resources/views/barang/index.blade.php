@extends('layout.master')
@section('judul')
    Halaman List Barang
@endsection

@section('content')

    <a href="/barang/create" class="btn btn-success my-3"> Tambah Barang </a>

    <table class="table">
        <thead class="thead-dark" align="center">
            <tr>
            <th scope="col" width="10%">No</th>
            <th scope="col" width="50%">Nama Barang</th>
            <th scope="col" width="20%">Kategori</th>
            <th scope="col" width="20%">Action</th>
            </tr>
        </thead>
        <tbody align="center">
            @forelse ($barang as $key => $item)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$item->nama_barang}}</td>
                    <td>{{$item->kategori->nama_kategori}}</td>
                    <td>
                        <form action="/barang/{{$item->id}}" method="post">
                            @csrf
                            @method('delete')
                            <a href="/barang/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/barang/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
               <h2 style="color: red;">Data Kosong</h2> 
            @endforelse
        </tbody>
    </table>
@endsection